<!doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>
    <div style="text-align:center">
      <h3> <br><br><br>Connexion</h3>
    </div>
    <div style="text-align:center", class="container" >
      <h2>Renseignez vos informations</h2>
        <form class="row g-3" action="tt_connexion.php" method="post"> 
        <div style="text-align:center", class="form-group" size="10">
          <label for="usr" size="10">Nom d'utilisateur:</label>
          <input type="mail" class="form-control" id="usr" size="10" required name="l_email">
        </div>
        <div style="text-align:center", class="form-group" size="10">
          <label for="pwd" size="10">Mot de passe:</label>
          <input type="password" class="form-control" id="pwd" size="10" required name="le_pass">
        </div>
        <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Connexion</button></div> 
      </form>
    </div>
    <div style="text-align:center">
      
      
    </div>
    
    <div style="text-align:center">
      <a href="index.php" ><input type="button" value="retour">
      </a>
    </div>

      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    </div>
  </body>
</html>