<?php
  // Initialiser la session
  session_start();
  // Vérifiez si l'utilisateur est connecté, sinon redirigez-le vers la page de connexion
  if(isset($_SESSION['admin'])!= 0 ){

?>

<doctype html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap demo</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>
    <div style="text-align:center">
        <FONT size ="1000px">
      Création de plat
      </FONT>
    </div>
    <div style="text-align:center", class="container" size="8">
        <h2>Remplissez les informations</h2>
        <div class="container">
          <form class="row g-3" action="tt_creerPlat.php" method="post"> 
            <div class="col-md-6">
              <label for="nom" class="form-label">Nom</label>
              <input type="text" class="form-control" id="nom" required name="le_nom">
            </div>
            <div class="col-md-6">
              <label for="desc" class="form-label">Description</label>
              <input type="text" class="form-control" id="desc" required name="la_desc">
            </div>
            <div class="col-md-6">
                <label for="cat" class="form-label">Catégorie</label>
                <input type="text" class="form-control" id="cat" required name="la_cat">
              </div>
              <div class="col-md-6">
                <label for="prix" class="form-label">Prix</label>
                <input type="text" class="form-control" id="prix" required name="le_prix">
              </div>
            <div class="row my-3">
          <div class="d-grid gap-2 d-md-block"><button class="btn btn-outline-primary" type="submit">Envoyer</button></div>   
        </div>
            
          </form>
        </div>

      </div>
      </div>
      <div style="text-align:center">
        <a href="listePlats.php" ><input type="button" value="retour">
        </a>
      </div>
    </body>


    <?php
    }
    else{
       echo "compte pas admin";
       //header('Location: connexion.php');
       echo '<div style="text-align:center">
         <a href="login.php" ><input type="button" value="Connexion">
         </a>
       </div>';
     
     }
       
   ?>