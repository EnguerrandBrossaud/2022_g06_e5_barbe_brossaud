<?php
  session_start();

  if(isset($_SESSION['admin'])!= 0 ){


  $nom =  htmlentities($_POST['le_nom']);
  $description = htmlentities($_POST['la_desc']);
  $cat = htmlentities($_POST['la_cat']);
  $prix = htmlentities($_POST['le_prix']);
  
  $options = [
        'cost' => 12,
  ];

  // Connexion :
  require_once("bdd.inc.php");
  $mysqli = new mysqli($host, $name, $passwd, $dbname);
  if ($mysqli->connect_error) {
      die('Erreur de connexion (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
  }

  if($_SESSION['admin'] == 1){
    if ($stmt = $mysqli->prepare("INSERT INTO plats(nom, description, cat, prix) VALUES (?, ?, ?, ?)")) {
      //echo "teste1";
      $stmt->bind_param("sssd", $nom, $description, $cat, $prix);
      //$sql = "INSERT INTO compte(username, mdp, isResp) VALUES ('$email', '$password', $role)";
      //echo $sql;
      if($stmt->execute()) {
          $_SESSION['message'] = "Enregistrement réussi";
      } else {
          $_SESSION['message'] =  "Impossible d'enregistrer";
      }
      //echo $_SESSION['message'];
    }
    header('Location: listePlats.php');
  }
  else{
    $_SESSION['message'] = "Seuls les administrateurs peuvent accéder à cette page";
  }

  
  }

?>