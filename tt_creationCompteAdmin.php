<?php
  session_start();

  if(isset($_SESSION['admin'])!= 0 ){

  $email =  htmlentities($_POST['l_email']);
  $password = htmlentities($_POST['le_pass']);
  $role = 1;
  
  $options = [
        'cost' => 12,
  ];

  // Connexion :
  require_once("bdd.inc.php");
  $mysqli = new mysqli($host, $name, $passwd, $dbname);
  if ($mysqli->connect_error) {
      die('Erreur de connexion (' . $mysqli->connect_errno . ') '
              . $mysqli->connect_error);
  }


  if ($stmt = $mysqli->prepare("INSERT INTO compte(username, mdp, isResp) VALUES (?, ?, ?)")) {
    //echo "teste1";
    $password = password_hash($password, PASSWORD_BCRYPT, $options);
    $stmt->bind_param("ssi", $email, $password, $role);
    //$sql = "INSERT INTO compte(username, mdp, isResp) VALUES ('$email', '$password', $role)";
    //echo $sql;
    if($stmt->execute()) {
        $_SESSION['message'] = "Enregistrement réussi";
    } else {
        $_SESSION['message'] =  "Impossible d'enregistrer";
    }
    //echo $_SESSION['message'];
  }

  header('Location: PageAdmin.php');
  }
  else{
    echo "compte pas admin";
    //header('Location: connexion.php');
    echo '<div style="text-align:center">
      <a href="login.php" ><input type="button" value="Connexion">
      </a>
    </div>';
  
  }
  
?>